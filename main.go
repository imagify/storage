package main

import (
	"flag"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	logging "gitlab.com/imagify/logging-lib"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"path"
	"syscall"
	"unicode"
)

func main() {
	var host string
	var isProduction bool
	flag.StringVar(&host, "host", "0.0.0.0:3001", "Sets host of microservice")
	flag.BoolVar(
		&isProduction, "production", false,
		"Determines if environment is production",
	)
	flag.Parse()
	log := logging.NewLogger(isProduction)
	app := fiber.New()
	go func() {
		app.Use(logger.New())
		app.Get("/download", func(ctx *fiber.Ctx) error {
			fileID := ctx.FormValue("file_id")
			switch isIDValid(fileID) {
			case 1:
				ctx.Status(400)
				return ctx.SendString("File ID is empty")
			case 2:
				ctx.Status(400)
				return ctx.SendString("File ID is larger than 32 chars")
			case 3:
				ctx.Status(400)
				return ctx.SendString("File ID does not contain only digits")
			}
			fp := path.Join(os.TempDir(), "imagify", "storage", fileID)
			_, err := os.Stat(fp)
			if err != nil {
				if os.IsNotExist(err) {
					log.Info("Requested non-existent file " + fileID)
					ctx.Status(400)
					return ctx.SendString("File not found")
				}
				ctx.Status(500)
				return ctx.SendString("Internal server error")
			}
			return ctx.SendFile(fp)
		})
		app.Post("/upload", func(ctx *fiber.Ctx) error {
			temp, err := os.CreateTemp("imagify/storage", "*")
			if err != nil {
				log.Error("Cannot create temp file", zap.Error(err))
				ctx.Status(500)
				return ctx.SendString("Internal server error")
			}
			_, err = temp.Write(ctx.Body())
			if err != nil {
				_ = temp.Close()
				err := os.Remove(temp.Name())
				if err != nil {
					log.Error("Cannot remove temp file", zap.Error(err))
				}
				log.Error("Cannot write to temp file", zap.Error(err))
				ctx.Status(500)
				return ctx.SendString("Internal server error")
			}
			return ctx.JSON(fiber.Map{
				"file_id": path.Base(temp.Name()),
			})
		})
		log.Fatal("App terminated", zap.Error(app.Listen(host)))
	}()
	sign := make(chan os.Signal)
	signal.Notify(sign, syscall.SIGINT)
	<-sign
	log.Info("Shutting down app")
	_ = app.Shutdown()
}

func isIDValid(value string) int {
	if len(value) == 0 {
		return 1
	}
	if len(value) > 32 {
		return 2
	}
	for _, c := range value {
		if !unicode.IsDigit(c) {
			return 3
		}
	}
	return 0
}
